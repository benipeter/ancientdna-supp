
sg10=["split/green2010/green2010"]
rule split_green2011:
    input: "raw/green2010.pdf"
    output: 
        expand("{sg10}_{num}.pdf", sg10=sg10, num=range(1,20))
    shell:
        "pdftk {input} cat 3-7 172-end output {output[0]} ;"
        "pdftk {input} cat 8-16 172-end output {output[1]} ;"
        "pdftk {input} cat 17-27 172-end output {output[2]} ;"
        "pdftk {input} cat 27-33 172-end output {output[3]} ;"
        "pdftk {input} cat 34-35 172-end output {output[4]} ;"
        "pdftk {input} cat 36-38 172-end output {output[5]} ;"
        "pdftk {input} cat 39-50 172-end output {output[6]} ;"
        "pdftk {input} cat 51-53 172-end output {output[7]} ;"
        "pdftk {input} cat 54-56 172-end output {output[8]} ;"
        "pdftk {input} cat 57-69 172-end output {output[9]} ;"
        "pdftk {input} cat 70-87 172-end output {output[10]} ;"
        "pdftk {input} cat 88-103 172-end output {output[11]} ;"
        "pdftk {input} cat 104-122 172-end output {output[12]} ;"
        "pdftk {input} cat 123-129 172-end output {output[13]} ;"
        "pdftk {input} cat 130-141 172-end output {output[14]} ;"
        "pdftk {input} cat 142-149 172-end output {output[15]} ;"
        "pdftk {input} cat 150-158 172-end output {output[16]} ;"
        "pdftk {input} cat 159-162 172-end output {output[17]} ;"
        "pdftk {input} cat 163-end output {output[18]} ;"

rule sort_green2010:
        input: expand("{sg10}_{num}.pdf", sg10=sg10, num=range(1,20))
        output: 
            "topics/wetlab/capture/g10s01_restriction_enzyme_enrichment.pdf",
            "topics/wetlab/sequencing/g10s02_extraction_libprep_seq.pdf", 
            "topics/bioinformatics/g10s03_alignment_filtering.pdf",
            "topics/bioinformatics/assembly/g10s04_mtdna_vindija3326.pdf",
            "topics/bioinformatics/contamination/g10s05_mtdna.pdf",
            "topics/bioinformatics/contamination/g10s06_ychr.pdf",
            "topics/bioinformatics/contamination/g10s07_nuclear.pdf",
            "topics/wetlab/sequencing/g10s08_additional_nea.pdf",
            "topics/wetlab/sequencing/g10s09_hgdp_5indiv.pdf",
            "topics/popgen/divergence/g10s10_nea_human_divergence.pdf",
            "topics/functional/catalog/g10s11_catalogue_unique_human.pdf",
            "topics/bioinformatics/genotyping/g10s12_cnv.pdf",
            "topics/popgen/selection/g10s13_sweep_screen.pdf",
            "topics/popgen/divergence/g10s14_nea_human_divergence.pdf",
            "topics/popgen/admixture/D/g10s15_nea_admixture_abba_baba.pdf",
            "topics/popgen/admixture/haplotypes/g10s16_nea_haplotypes_humans.pdf",
            "topics/popgen/admixture/haplotypes/g10s17_nea_haplotypes_non_afr.pdf",
            "topics/popgen/admixture/f4ratio/g10s18_nea_proportion.pdf",
            "topics/popgen/admixture/D/g10s19_nea_fraction.pdf",
        run:
            for i in range(len(input)):
                a, b = input[i], output[i]
                shell("ln -sfr {a} {b}")

sr10=["split/reich2010/reich2010"]
rule split_reich2010:
    input: "raw/reich2010.pdf"
    output: 
        expand("{sr10}_{num}.pdf", sr10=sr10, num=range(1,13+1))
    shell:
        "pdftk {input} cat 2-3 output {output[0]} ;"
        "pdftk {input} cat 4-10 output {output[1]} ;"
        "pdftk {input} cat 11-15 output {output[2]} ;"
        "pdftk {input} cat 16-25 output {output[3]} ;"
        "pdftk {input} cat 26-31 output {output[4]} ;"
        "pdftk {input} cat 32-37 output {output[5]} ;"
        "pdftk {input} cat 38-46 output {output[6]} ;"
        "pdftk {input} cat 47-58 output {output[7]} ;"
        "pdftk {input} cat 59-60 output {output[8]} ;"
        "pdftk {input} cat 61-68 output {output[9]} ;"
        "pdftk {input} cat 69-80 output {output[10]} ;"
        "pdftk {input} cat 81-86 output {output[11]} ;"
        "pdftk {input} cat 87-90 output {output[12]} ;"

rule sort_reich2010:
        input: expand("{sr10}_{num}.pdf", sr10=sr10, num=range(1,14))
        output: 
            "topics/wetlab/sequencing/r10s01_denisova_sequencing.pdf",
            "topics/popgen/divergence/r10s02_denisova_divergence.pdf",
            "topics/bioinformatics/contamination/r10s03_deni_contamination.pdf",
            "topics/functional/catalog/r10s04_deni_catalog.pdf",
            "topics/bioinformatics/genotyping/r10s05_cnv.pdf",
            "topics/popgen/divergence/r10s06_deni_nea_divergence.pdf",
            "topics/popgen/admixture/D/r10s07_deni_nea.pdf",
            "topics/popgen/admixture/D/r10s08_deni_melanesia.pdf",
            "topics/wetlab/sequencing/r10s09_modern_humans.pdf",
            "topics/popgen/admixture/D/r10s10_robustness.pdf",
            "topics/popgen/admixture/D/r10s11_deni_parameter_estimates.pdf",
            "topics/archaeology/morphology/r10s12_deni_molar.pdf",
            "topics/wetlab/sequencing/r10s13_deni_molar_mtdna.pdf",
        run:
            for i in range(13):
                a, b = input[i], output[i]
                shell("ln -sfr {a} {b}")

srm10=["split/rasmussen2010/rasmussen2010"]
rule split_rasmussen2010:
    input: "raw/rasmussen2010.pdf"
    output: 
        expand("{name}_{num}.pdf", name=srm10, num=range(1,22+1))
    shell:
        "pdftk {input} cat 2-3 46-end output {output[0]} ;"
        "pdftk {input} cat 3-6 46-end output {output[1]} ;"
        "pdftk {input} cat 6-8 46-end output {output[2]} ;"
        "pdftk {input} cat 8-10 46-end output {output[3]} ;"
        "pdftk {input} cat 10-12 46-end output {output[4]} ;"
        "pdftk {input} cat 12 46-end output {output[5]} ;"
        "pdftk {input} cat 12-14 46-end output {output[6]} ;"
        "pdftk {input} cat 14-15 46-end output {output[7]} ;"
        "pdftk {input} cat 15-19 46-end output {output[8]} ;"
        "pdftk {input} cat 19-22 46-end output {output[9]} ;"
        "pdftk {input} cat 22-27 46-end output {output[10]} ;"
        "pdftk {input} cat 27 46-end output {output[11]} ;"
        "pdftk {input} cat 27-31 46-end output {output[12]} ;"
        "pdftk {input} cat 31-33 46-end output {output[13]} ;"
        "pdftk {input} cat 33-34 46-end output {output[14]} ;"
        "pdftk {input} cat 34-37 46-end output {output[15]} ;"
        "pdftk {input} cat 37-38 46-end output {output[16]} ;"
        "pdftk {input} cat 38-40 46-end output {output[17]} ;"
        "pdftk {input} cat 40-42 46-end output {output[18]} ;"
        "pdftk {input} cat 42-43 46-end output {output[19]} ;"
        "pdftk {input} cat 43-44 46-end output {output[20]} ;"
        "pdftk {input} cat 44-46 46-end output {output[21]} ;"

rule sort_rasmussen2010:
        input: rules.split_rasmussen2010.output
        output: 
            "topics/archaeology/context/ra10s01_saqqaq_context.pdf",
            "topics/archaeology/morphology/ra10s02_saqqaq_hair.pdf",
            "topics/archaeology/dating/ra10s03_saqqaq_dating.pdf",
            "topics/archaeology/dating/ra10s04_saqqaq_stable_isotope.pdf",
            "topics/wetlab/sequencing/ra10s05_saqqaq_sample_prep.pdf",
            "topics/wetlab/sequencing/ra10s06_saqqaq_sequencing.pdf",
            "topics/wetlab/sequencing/ra10s07_saqqaq_dmg_pattern.pdf",
            "topics/wetlab/sequencing/ra10s08_saqqaq_ychr.pdf",
            "topics/bioinformatics/assembly/ra10s09_saqqaq_assembly.pdf",
            "topics/bioinformatics/genotyping/ra10s10_SNPest.pdf",
            "topics/bioinformatics/contamination/ra10s11_saqqaq_xchr.pdf",
            "topics/bioinformatics/genotyping/ra10s12_mtdna.pdf",
            "topics/bioinformatics/contamination/ra10s13_saqqaq_european.pdf",
            "topics/popgen/ychr/ra10s14_ychr.pdf",
            "topics/popgen/multivariate/ra10s15_saqqaq_pca.pdf",
            "topics/popgen/inbreeding/ra10s16_saqqaq_inbreeding.pdf",
            "topics/popgen/inbreeding/ra10s17_saqqaq_inbreeding_hmm.pdf",
            "topics/popgen/divergence/ra10s18_saqqaq_divergence.pdf",
            "topics/popgen/multivariate/ra10s19_saqqaq_structure.pdf",
            "topics/other_data/ra10s20_modern_humans.pdf",
            "topics/other_data/ra10s21_metagenomics.pdf",
            "topics/functional/go/ra10s22_saqqaq_go.pdf",
        run:
            for i in range(len(output)):
                a, b = input[i], output[i]
                shell("ln -sfr {a} {b}")

srm11=["split/rasmussen2011/rasmussen2011"]
rule split_rasmussen2011:
    input: "raw/rasmussen2011.pdf"
    output: 
        expand("{name}_{num}.pdf", name=srm11, num=range(1,19+1)),
        nocover=temp("tmp/rasmussen2011_no_cover.pdf")
    shell:
        "pdftk {input} cat 2-end output {output.nocover} ;"
        "pdftk {output.nocover} cat 3-6  output {output[0]} ;"
        "pdftk {output.nocover} cat 6-8 75 136-145 71-74  output {output[1]} ;" #S1-S10 T1
        "pdftk {output.nocover} cat 8-10 76 146 71-74 output {output[2]} ;" #S11 T2
        "pdftk {output.nocover} cat 10-11 71-74 output {output[3]} ;"
        "pdftk {output.nocover} cat 11-13 71-74 output {output[4]} ;"
        "pdftk {output.nocover} cat 13-14 77-79 147 71-74 output {output[5]} ;" #S12 T3-5
        "pdftk {output.nocover} cat 14-17 80 148 71-74 output {output[6]} ;"#S13 T6
        "pdftk {output.nocover} cat 17-20 149-150 71-74 output {output[7]} ;" #S14-15
        "pdftk {output.nocover} cat 20-21 81-82 71-74 output {output[8]} ;" #T7-8
        "pdftk {output.nocover} cat 21-22 83 151-153 71-74 output {output[9]} ;" #S16-18 T9
        "pdftk {output.nocover} cat 22-25 84-85 71-74 output {output[10]} ;" #T10-T11
        "pdftk {output.nocover} cat 25-31 86-88 154-155 71-74 output {output[11]} ;" #S19-20 T12-14
        "pdftk {output.nocover} cat 31-34 89 156 71-74 output {output[12]} ;"#S21 T15
        "pdftk {output.nocover} cat 34-40 90 157-165 71-74 output {output[13]} ;"#S22-30 T16
        "pdftk {output.nocover} cat 40-49 91-92 166-169 71-74 output {output[14]} ;"# S31-34 T17-18
        "pdftk {output.nocover} cat 49-60 93-95 170-171 71-74 output {output[15]} ;"# S35-S36 T19-21
        "pdftk {output.nocover} cat 60-61 172-173 71-74 output {output[16]} ;"# S37-S38
        "pdftk {output.nocover} cat 61-65 96-133 174 71-74 output {output[17]} ;"# S39 T22-26
        "pdftk {output.nocover} cat 65-67 134-135 71-74 output {output[18]} ;"#T27-8

rule sort_rasmussen2011:
        input: rules.split_rasmussen2011.output
        output: 
            "topics/other_data/r11s01_terms.pdf",
            "topics/archaeology/morphology/r11s02_hair.pdf",
            "topics/archaeology/dating/r11s03_isotopes.pdf",
            "topics/wetlab/r11s04_abo_seq.pdf",
            "topics/bioinformatics/contamination/r11s05_filtering.pdf",
            "topics/bioinformatics/alignment/r11s06_alignment.pdf",
            "topics/bioinformatics/genotyping/r11s07_genotyping_samtools.pdf",
            "topics/bioinformatics/damage/r11s08_dmg_estimates.pdf",
            "topics/other_data/r11s09_gt_other_data.pdf",
            "topics/bioinformatics/alignment/r11s10_non_human.pdf",
            "topics/bioinformatics/assembly/r11s11_abor_denovo.pdf",
            "topics/bioinformatics/contamination/r11s12_contamination.pdf",
            "topics/popgen/singlelocus/r11s13_abor_y_mtdna.pdf",
            "topics/popgen/multivariate/r11s14_abor_autosomal.pdf",
            "topics/popgen/demography/r11s15_abor_pairwise_coal.pdf",
            "topics/popgen/admixture/D/r11s16_D4P.pdf",
            "topics/popgen/admixture/r11s17_tag_snp.pdf",
            "topics/popgen/admixture/D/r11s18_aborigines_Dstats.pdf",
            "topics/bioinformatics/error/r11s19_aborigines_relerror.pdf",
        run:
            for i in range(len(output)):
                a, b = input[i], output[i]
                shell("ln -sfr {a} {b}")


srm11=["split/fu2013/fu2013"]
rule split_fu13:
    input: "raw/fu2013.pdf"
    output: 
        expand("{name}_{num}.pdf", name=srm11, num=range(1,7+1))
    shell:
        "pdftk {input} cat 7-end  output {output[0]} ;"
        "pdftk {input} cat 7-end  output {output[1]} ;" 
        "pdftk {input} cat 7-end  output {output[2]} ;"
        "pdftk {input} cat 9-end output {output[3]} ;" 
        "pdftk {input} cat 9-end  output {output[4]} ;"
        "pdftk {input} cat 10-end output {output[5]} ;" 
        "pdftk {input} cat 10-end output {output[6]} ;" 

rule sort_fu2013:
        input: rules.split_rasmussen2011.output
        output: 
            "topics/wetlab/sequencing/fu13s01_tianyuan_shotgun.pdf",
            "topics/bioinformatics/assembly/fu13s02_tianyuan_mtdna.pdf",
            "topics/wetlab/capture/fu13s03_chr21capture.pdf",
            "topics/bioinformatics/test/fu13s3.3_enrichment_efficiency.pdf",
            "topics/bioinformatics/contamination/fu13s3.4_coverage_contamination.pdf",
            "topics/popgen/tree/fu13s3.6_tianyuan_treemix.pdf",
            "topics/popgen/admixture/fu13s3.7_tianyuan_admixture_dist.pdf"
        run:
            for i in range(len(output)):
                a, b = input[i], output[i]
                shell("ln -sfr {a} {b}")

ca10=["split/castellano2014/castellano2014"]
rule split_castellano2014:
    input: "raw/castellano2014.pdf"
    output: 
        expand("{sg10}_{num}.pdf", sg10=ca10, num=range(1,12+1))
    shell:
        "pdftk {input} cat 3-4 25-end output {output[0]} ;"
        "pdftk {input} cat 4-5 25-end output {output[1]} ;"
        "pdftk {input} cat 5 25-end output {output[2]} ;"
        "pdftk {input} cat 5-6 25-end output {output[3]} ;"
        "pdftk {input} cat 6-10 25-end output {output[4]} ;"
        "pdftk {input} cat 10 25-end output {output[5]} ;"
        "pdftk {input} cat 10-12 25-end output {output[6]} ;"
        "pdftk {input} cat 12-14 25-end output {output[7]} ;"
        "pdftk {input} cat 14 25-end output {output[8]} ;"
        "pdftk {input} cat 14-16 25-end output {output[9]} ;"
        "pdftk {input} cat 16-17 25-end output {output[10]} ;"
        "pdftk {input} cat 17-end output {output[11]} ;"

rule sort_castellano2014:
        input: expand("{sg10}_{num}.pdf", sg10=sg10, num=range(1,13))
        output: 
            "topics/wetlab/sequencing/c14s01_extraction_libprep.pdf",
            "topics/bioinformatics/capture/c14s02_exome_array_design.pdf", 
            "topics/wetlab/capture/c14s03_exome_capture.pdf",
            "topics/bioinformatics/alignment/c14s04_exome_base_calling_mapping.pdf",
            "topics/bioinformatics/capture/c14s05_exome_capture_qc.pdf",
            "topics/bioinformatics/contamination/c14s06_contamination.pdf",
            "topics/bioinformatics/genotyping/c14s07_exome_genotyping.pdf",
            "topics/popgen/multivariate/c14s08_exome_tree_pca_fst.pdf",
            "topics/popgen/het/c14s09_exome_het_est.pdf",
            "topics/functional/conservation/c14s10_exome_lineage_specific_allele_func.pdf",
            "topics/popgen/selection/c14s11_deleteriousness.pdf",
            "topics/functional/catalog/c14s12_exome_catalog.pdf",
        run:
            for i in range(len(input)):
                a, b = input[i], output[i]
                shell("ln -sfr {a} {b}")

slon2017=["split/slon2017/slon2017"]
rule split_slon2017:
    input: "raw/slon2017.pdf"
    output: 
        expand("{name}_{num}.pdf", name=slon2017, num=range(1,4+1)),
    shell:
        "pdftk {input} cat 3-4 12-14 21 output {output[0]} ;"
        "pdftk {input} cat 5-7 15-16 22-23 output {output[1]} ;"
        "pdftk {input} cat 8-9 17-18 24 output {output[2]} ;"
        "pdftk {input} cat 10-11 19-20 25-end output {output[3]} ;"

rule sort_slon2017:
        input: rules.split_slon2017.output
        output: 
           "topics/archaeology/morphology/sl17s01_denisova2_morphology.pdf",
           "topics/bioinformatics/contamination/sl17s02_denisova2_authentication.pdf",
           "topics/bioinformatics/mtdna/sl17s03_denisova2_mtdna.pdf",
           "topics/popgen/admixture/D/sl17s04_denisova2.pdf",
        run:
            for i in range(len(output)):
                a, b = input[i], output[i]
                shell("ln -sfr {a} {b}")

lazaridis2014=["split/lazaridis2014/lazaridis2014"]
rule split_lazaridis2014:
    input: "raw/lazaridis2014.pdf"
    output: 
        expand("{name}_{num}.pdf", name=lazaridis2014, num=range(1,19+1)),
    shell:
        "pdftk {input} cat 2-8 output {output[0]} ;"
        "pdftk {input} cat 9-16 output {output[1]} ;"
        "pdftk {input} cat 17-23 output {output[2]} ;"
        "pdftk {input} cat 24-29 output {output[3]} ;"
        "pdftk {input} cat 30-36 output {output[4]} ;"
        "pdftk {input} cat 37-38 output {output[5]} ;"
        "pdftk {input} cat 39-41 output {output[6]} ;"
        "pdftk {input} cat 42-53 output {output[7]} ;"
        "pdftk {input} cat 54-70 output {output[8]} ;"
        "pdftk {input} cat 71-81 output {output[9]} ;"
        "pdftk {input} cat 82-85 output {output[10]} ;"
        "pdftk {input} cat 86-89 output {output[11]} ;"
        "pdftk {input} cat 90-93 output {output[12]} ;"
        "pdftk {input} cat 94-129 output {output[13]} ;"
        "pdftk {input} cat 130-134 output {output[14]} ;"
        "pdftk {input} cat 135-143 output {output[15]} ;"
        "pdftk {input} cat 144-147 output {output[16]} ;"
        "pdftk {input} cat 148-151 output {output[17]} ;"
        "pdftk {input} cat 152-155 output {output[18]} ;"

rule sort_lazaridis2014:
        input: rules.split_lazaridis2014.output
        output: 
            "topics/wetlab/sequencing/l14s01_sampling_libprep_seq.pdf",
            "topics/bioinformatics/genotyping/l14s02_genotyping_het.pdf",
            "topics/bioinformatics/authenticity/l14s03_authenticity.pdf",
            "topics/popgen/singlelocus/l14s04_lazaridis_mtdna.pdf",
            "topics/popgen/singlelocus/l14s05_lazaridis_sexdet_ychr.pdf",
            "topics/popgen/admixture/f4ratio/l14s06_lazaridis_f4_ratio.pdf",
            "topics/bioinformatics/genotyping/l14s07_cnv.pdf",
            "topics/functional/l14s08_phenotype_prediction.pdf",
            "topics/other_data/l14s09_human_origins_array.pdf",
            "topics/popgen/multivariate/l14s10_human_origins_pca.pdf",
            "topics/popgen/admixture/f3/l14s11_lazaridis_f3.pdf",
            "topics/popgen/admixture/number_of_sources/l14s12_lazaridis.pdf",
            "topics/popgen/admixture/f4ratio/l14s13_stuttgart.pdf",
            "topics/popgen/tree/l14s14_qpgraph.pdf",
            "topics/popgen/tree/l14s15_mixmapper.pdf",
            "topics/popgen/tree/l14s16_treemix.pdf",
            "topics/popgen/qpadm/l14s17_qpadm.pdf",
            "topics/popgen/ibd/l14s18_ibd_segments.pdf",
            "topics/popgen/multivariate/l14s19_lazaridis_finestructure.pdf",
        run:
            for i in range(len(output)):
                a, b = input[i], output[i]
                shell("ln -sfr {a} {b}")

pr10=["split/prufer14/prufer14"]
rule split_prufer2014:
    input: "raw/prufer2014.pdf"
    output: 
        expand("{name}_{num}.pdf", name=pr10, num=range(1,25+1)),
        nocover=temp("tmp/prufer2014_no_cover.pdf")
    shell:
        "pdftk {input} cat 2-end output {output.nocover} ;"
        "pdftk {output.nocover} cat 1-5  output {output[0]} ;"
        "pdftk {output.nocover} cat 6-8  output {output[1]} ;"
        "pdftk {output.nocover} cat 9-13  output {output[2]} ;"
        "pdftk {output.nocover} cat 14-15  output {output[3]} ;"
        "pdftk {output.nocover} cat 16-21  output {output[4]} ;"
        "pdftk {output.nocover} cat 22-33  output {output[5]} ;"
        "pdftk {output.nocover} cat 34-38  output {output[6]} ;"
        "pdftk {output.nocover} cat 39-48  output {output[7]} ;"
        "pdftk {output.nocover} cat 49-54  output {output[8]} ;"
        "pdftk {output.nocover} cat 55-57  output {output[9]} ;"
        "pdftk {output.nocover} cat 58-65  output {output[10]} ;"
        "pdftk {output.nocover} cat 66-69  output {output[11]} ;"
        "pdftk {output.nocover} cat 70-82  output {output[12]} ;"
        "pdftk {output.nocover} cat 83-88  output {output[13]} ;"
        "pdftk {output.nocover} cat 89-94  output {output[14]} ;"
        "pdftk {output.nocover} cat 95-119  output {output[15]} ;"
        "pdftk {output.nocover} cat 120-129  output {output[16]} ;"
        "pdftk {output.nocover} cat 130-138  output {output[17]} ;"
        "pdftk {output.nocover} cat 139-165  output {output[18]} ;"
        "pdftk {output.nocover} cat 166-179  output {output[19]} ;"
        "pdftk {output.nocover} cat 180-189  output {output[20]} ;"
        "pdftk {output.nocover} cat 190-215  output {output[21]} ;"
        "pdftk {output.nocover} cat 216-227  output {output[22]} ;"
        "pdftk {output.nocover} cat 228-244  output {output[23]} ;"
        "pdftk {output.nocover} cat 245-end  output {output[24]} ;"

rule sort_prufer2014:
        input: rules.split_prufer2014.output
        output: 
            "topics/wetlab/sequencing/pr14s01_altainea_sampling_libprep_seq.pdf",
            "topics/bioinformatics/processing/pr14s02a_altainea_processing_mapping.pdf",
            "topics/popgen/singlelocus/pr14s02b_altainea_mtdna.pdf",
            "topics/bioinformatics/genotyping/pr14s03_altainea_genotypcing.pdf",
            "topics/other_data/pr15s04_bteam.pdf",
            "topics/bioinformatics/qc/pr15s05a_altainea_genomedataquality.pdf",
            "topics/bioinformatics/qc/pr15s05b_altainea_filtering.pdf",
            "topics/popgen/divergence/pr15s06a_altainea_divergence.pdf",
            "topics/popgen/branchshortening/pr14s06b_altainea_branch_shortening.pdf",
            "topics/popgen/tree/pr14s07_altainea_treemix.pdf",
            "topics/bioinformatics/genotyping/pr14s08_altainea_cnv.pdf",
            "topics/popgen/het/pr14s09_altainea_het.pdf",
            "topics/popgen/inbreeding/pr14s10_altainea_inbreeding.pdf",
            "topics/popgen/xchr/pr14s11_altainea_xchr.pdf",
            "topics/popgen/demography/pr14s12_altainea_psmc.pdf",
            "topics/popgen/admixture/haplotypes/pr14s13_altai_sriram_method.pdf",
            "topics/popgen/admixture/D/pr14s14_altai_dstats.pdf",
            "topics/popgen/admixture/D/pr14s15_altai_denisova.pdf",
            "topics/popgen/admixture/D/pr14s16a_ghost.pdf",
            "topics/popgen/admixture/D/pr14s16b_ghost2_archaic_ancestry.pdf",
            "topics/popgen/admixture/D/pr14s17_d_modelling.pdf",
            "topics/functional/catalog/pr14s18_changes_modern_archaic.pdf",
            "topics/popgen/selection/pr14s19a_sweep_human_lineage.pdf",
            "topics/popgen/selection/pr14s19b_sweep_screen.pdf",
            "topics/functional/expression/pr14s20_brain_expression.pdf"
        run:
            for i in range(len(output)):
                a, b = input[i], output[i]
                shell("ln -sfr {a} {b}")


sawyer2015=["split/sawyer15/sawyer15"]
rule split_sawyer15:
    input: "raw/sawyer2015.pdf"
    output: 
        expand("{name}_{num}.pdf", name=sawyer2015, num=range(1,6+1)),
    shell:
        "pdftk {input} cat 1-6 38-end output {output[0]} ;"
        "pdftk {input} cat 7-8 38-end output {output[1]} ;"
        "pdftk {input} cat 9-10 38-end output {output[2]} ;"
        "pdftk {input} cat 11-18 38-end output {output[3]} ;"
        "pdftk {input} cat 19-23 38-end output {output[4]} ;"
        "pdftk {input} cat 24-end output {output[5]} ;"

rule sort_sawyer15:
        input: rules.split_lazaridis2014.output
        output: 
            "topics/archaeology/morphology/s15s01_denisova8_morphology.pdf",
            "topics/wetlab/sequencing/s15s02_extraction_prep_sequencing.pdf",
            "topics/bioinformatics/processing/s15s03_processing_mapping.pdf",
            "topics/bioinformatics/authenticity/s15s04_denisova4_8_authenticity.pdf",
            "topics/popgen/singlelocus/s15s05_mtdna.pdf",
            "topics/popgen/divergence/s15s06_denisova4_8_divergence_dstat.pdf",
        run:
            for i in range(len(output)):
                a, b = input[i], output[i]
                shell("ln -sfr {a} {b}")

rule meyer16:
    input: "raw/meyer2016.pdf"
    output:
       s1="split/meyer2016/meyer2016_1.pdf" ,
       s2="split/meyer2016/meyer2016_2.pdf" ,
       s3="split/meyer2016/meyer2016_3.pdf" ,
       t1="topics/bioinformatics/qc/m16s01_sima_read_length.pdf",
       t2="topics/bioinformatics/contamination/m16s02_cond_sub_freq.pdf",
       t3="topics/bioinformatics/contamination/m16s03_sima_highfreq.pdf",
    shell:
        "pdftk {input} cat 1-4 12 output {output.s1} ;"
        "pdftk {input} cat 5-8 12 output {output.s2} ;"
        "pdftk {input} cat 9-12 output {output.s3} ;"
        "ln -sfr {output.s1} {output.t1}; "
        "ln -sfr {output.s2} {output.t2}; "
        "ln -sfr {output.s3} {output.t3}; "

xxxxyyyy=["split/xxxxyyyy/xxxxyyyy"]
rule split_xxxxyyyy:
    input: "raw/x.pdf"
    output: 
        expand("{name}_{num}.pdf", name=xxxxyyyy, num=range(1,6+1)),
    shell:
        "pdftk {input} cat 7-8 38-end output {output[1]} ;"

rule sort_xxxxyyyy:
        input: rules.split_xxxxyyyy.output
        output: 
            "",
        run:
            for i in range(len(output)):
                a, b = input[i], output[i]
                shell("ln -sfr {a} {b}")

prufer2017=["split/pruferx2017/pruferx2017"]
rule split_prufer2017:
    input: "raw/prufer2017.pdf"
    output: 
        expand("{name}_{num}.pdf", name=prufer2017, num=range(1,16+1)),
    shell:
        "pdftk {input} cat 3-4 169-171 227-end output {output[0]} ;" #t1-t3
        "pdftk {input} cat 5-7 172-175 66-69 227-end output {output[1]} ;" #s1-s4, t4-t6
        "pdftk {input} cat 8-13 176-178  70-74 227-end output {output[2]} ;" #s5-s9, t7-t9
        "pdftk {input} cat 14-15 179-182  227-end output {output[3]} ;" #t10-t13
        "pdftk {input} cat 16-18 183-184 75-80 227-end output {output[4]} ;"#s10-s15,t14-t15
        "pdftk {input} cat 19-20 185-186 81 227-end output {output[5]} ;"#s16, t16-t17
        "pdftk {input} cat 21-25 187-191 82-91 227-end output {output[6]} ;"#s17-s26,t18-t22
        "pdftk {input} cat 26-31 92-99  227-end output {output[7]} ;"#s27-s34,
        "pdftk {input} cat 32-37 192-195 100-111 227-end output {output[8]} ;"#s35-s46,t23-t26
        "pdftk {input} cat 38-44 196-198 112-131 227-end output {output[9]} ;"#s47-s66,t27-t29
        "pdftk {input} cat 45-47 199 132-136 227-end output {output[10]} ;"#s67-s71,t30-
        "pdftk {input} cat 48-50 200-206  227-end output {output[11]} ;" #t31-t35
        "pdftk {input} cat 51-53 207-208 137-141 227-end output {output[12]} ;"#s72-s76,t36-t37
        "pdftk {input} cat 54-58 209-211 142-158 227-end output {output[13]} ;"#s77-s93,t38-t40
        "pdftk {input} cat 59-62 212-215 226 159-166 227-end output {output[14]};" #s94-s101,t41-t43,t52
        "pdftk {input} cat 63-end 216-225 167-168 227-end output {output[15]} ;"#s102-s103 t44-t51

rule sort_prufer2017:
        input: rules.split_prufer2017.output
        output: 
            "topics/wetlab/p17s01_sampling_dating_extraction_libprep.pdf",
            "topics/bioinformatics/alignment/p17s02_vindija_processing.pdf",
            "topics/bioinformatics/p17s03_genotyping.pdf",
            "topics/bioinformatics/contamination/p17s04_vindija_contamination.pdf",
            "topics/popgen/het/p17s05_vindija_het_inbreeding.pdf",
            "topics/popgen/divergence/p17s06_other_vindija.pdf",
            "topics/popgen/demography/p17s07_vindija_psmc_branch_shortening.pdf",
            "topics/popgen/admixture/D/p17s08_vindija_modern_humans.pdf",
            "topics/popgen/admixture/D/p17s09a_human_introgression_into_nt.pdf",
            "topics/popgen/admixture/D/p17s09b_stratified_d.pdf",
            "topics/popgen/divergence/p17s10_relationship_deni_neandertal.pdf",
            "topics/popgen/divergence/p17s11_relationship_mezmaiskaya_other_nea.pdf",
            "topics/popgen/admixture/sstar/p17s12_sstar.pdf",
            "topics/bioinformatics/genotyping/p17s13_vindija_structural_var.pdf",
            "topics/bioinformatics/genotyping/p17s14_cnv.pdf",
            "topics/functional/catalog/p17s15_catalog.pdf",
        run:
            for i in range(len(output)):
                a, b = input[i], output[i]
                shell("ln -sfr {a} {b}")
rule all:
    input:
        rules.sort_reich2010.output,
        rules.sort_green2010.output,
        rules.sort_rasmussen2010.output,
        rules.sort_rasmussen2011.output,
        rules.sort_fu2013.output,
        rules.sort_castellano2014.output,
        rules.sort_slon2017.output,
        rules.sort_lazaridis2014.output,
        rules.sort_prufer2014.output,
        rules.sort_sawyer15.output,
        rules.meyer16.output,
        rules.sort_prufer2017.output,
