mkdir -p raw/ papers/
wget -qc -nc -O papers/green2010.pdf http://science.sciencemag.org/content/328/5979/710.full.pdf 
wget -qc -nc -O raw/green2010.pdf http://science.sciencemag.org/highwire/filestream/591549/field_highwire_adjunct_files/0/Green_SOM.pdf
wget -qc -nc -O raw/burbano2010.pdf http://science.sciencemag.org/highwire/filestream/591557/field_highwire_adjunct_files/0/Burbano.SOM.pdf
wget -qc -nc -O papers/burbano2010.pdf http://science.sciencemag.org/content/328/5979/723.full.pdf
wget -qc -nc -O papers/reich2010.pdf https://www.nature.com/articles/nature09710.pdf
wget -qc -nc -O raw/reich2010.pdf https://images.nature.com/original/nature-assets/nature/journal/v468/n7327/extref/nature09710-s1.pdf
wget -qc -nc -O papers/marciniak2017.pdf https://www.nature.com/articles/nrg.2017.65.pdf
wget -qc -nc -O papers/rasmussen2010.pdf https://www.nature.com/articles/nature08835.pdf
wget -qc -nc -O raw/rasmussen2010.pdf https://images.nature.com/original/nature-assets/nature/journal/v463/n7282/extref/nature08835-s1.pdf
wget -qc -nc -O raw/rasmussen2011.pdf http://science.sciencemag.org/content/sci/suppl/2011/09/21/science.1211177.DC1/Rasmussen.SOM.pdf
wget -qc -nc -O raw/keller2012.pdf https://images.nature.com/original/nature-assets/ncomms/journal/v3/n2/extref/ncomms1701-s1.pdf
wget -qc -nc -O raw/fu2013.pdf http://www.pnas.org/content/110/6/2223.full.pdf
wget -qc -nc -O papers/fu2013.pdf http://www.pnas.org/content/110/6/2223.full.pdf
wget -qc -nc -O raw/castellano2014.pdf http://www.pnas.org/content/suppl/2014/04/17/1405138111.DCSupplemental/pnas.1405138111.sapp.pdf
wget -qc -nc -O papers/castellano2014.pdf https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4020111/pdf/pnas.201405138.pdf
wget -qc -nc -O papers/slon2017.pdf http://advances.sciencemag.org/content/3/7/e1700186.full.pdf
wget -qc -nc -O raw/slon2017.pdf http://advances.sciencemag.org/highwire/filestream/196784/field_highwire_adjunct_files/0/1700186_SM.pdf
wget -qc -nc -O papers/slon2017b.pdf http://science.sciencemag.org/content/early/2017/04/26/science.aam9695.full.pdf
wget -qc -nc -O raw/slon2017b.pdf http://science.sciencemag.org/content/sci/suppl/2017/04/26/science.aam9695.DC1/aam9695_SM.pdf
wget -qc -nc -O raw/lazaridis2014.pdf https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4170574/bin/NIHMS613260-supplement-supplement_1.pdf
wget -qc -nc -O papers/lazaridis2014.pdf https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4170574/pdf/nihms613260.pdf
wget -qc -nc -O papers/prufer2014.pdf https://www.nature.com/articles/nature12886.pdf
wget -qc -nc -O raw/prufer2014.pdf https://images.nature.com/original/nature-assets/nature/journal/v505/n7481/extref/nature12886-s1.pdf

wget -qc -nc -O papers/sawyer2015.pdf https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4697428/pdf/pnas.201519905.pdf
wget -qc -nc -O raw/sawyer2015.pdf http://www.pnas.org/content/suppl/2015/11/11/1519905112.DCSupplemental/pnas.1519905112.sapp.pdf

wget -qc -nc -O papers/meyer2016.pdf https://www.nature.com/articles/nature17405.pdf
wget -qc -nc -O raw/meyer2016.pdf https://images.nature.com/original/nature-assets/nature/journal/v531/n7595/extref/nature17405-s1.pdf

wget -qc -nc -O papers/prufer2017.pdf http://science.sciencemag.org/content/early/2017/10/04/science.aao1887.full.pdf
wget -qc -nc -O raw/prufer2017.pdf http://science.sciencemag.org/highwire/filestream/700025/field_highwire_adjunct_files/0/aao1887_Prufer_SM.pdf
