#AncientDNA supplement repository

The goal of this repo is to organize and annotate the supplementary Materials
for ancientDNA-related papers. The main reason I believe this is necessary is that these
supplements are 

- fundamental for understanding the progress and ideas in the field
- poorly indexed by both generic and scientific search engines
- often tangential to the main purpose of the paper

Therefore, I split up the large PDF-files into smaller files each containing
a single supplement, and sort them by topic, rather than publication. This makes
it hopefully easier to find all relevant work on any particular topic.

This is still a work in progress, and only a small number of papers have been
indexed, focusing on archaic and early ancient modern humans. The list of papers
is taken from table 1 of [papers/marciniak2010.pdf](Marciniak & Perry 2017, NRG)

## Table of content
 - wetlab
    - capture / enrichment
    - sequencing / library prep
 - bioinformatics
    - alignment
    - assembly
    - contamination
    - genotyping
 - popgen
    - admixture
       - ABBA-BABA
       - F4-ratio
       - Neandertal haplotypes
    - selection
    - divergence estimate
    - ychr
    - multivariate
    - inbreeding
 - functional genetics
    - catalog
    - gene ontology
 - archaeology
    - morphology
    - dating
 - other data
        

The following papers are currently indexed:

 - [papers/green2010.pdf](Green et al. 2010) Vindija Neandertal draft
 - [papers/burbano2010.pdf](Burbano et al. 2010 (no supp indexed))
 - [papers/reich2010.pdf](Reich et al. 2010) Denisova paper
 - [papers/rasmussen2010.pdf](Rasmussen et al. 2010) Saqqaq
 - [papers/rasmussen2011.pdf](Rasmussen et al. 2011) Aboriginal Australian
 - [papers/keller2012.pdf](Keller et al. 2012 (no supp texts))
 - [papers/skoglund2012.pdf](Skoglund et al. 2012) (not indexed)
 - [papers/fu2013.pdf](Fu et al. 2013)
 - [papers/castellano2014.pdf](Castellano et al. 2014)
 - [papers/slon2017.pdf](Slon et al. 2017) Denisova 2 paper
 - [papers/slon2017b.pdf](Slon et al. 2017b (Sediment paper)) (small supp. not
   indexed)
 - [papers/lazaridis2014.pdf](Lazaridis et al. 2014) large human origins array data]
 - [papers/prufer2014.pdf](Prüfer et al. 2014)
 - [papers/sawyer2015.pdf](Sawyer et al. 2015) second Denisovan
- Sawyer et al. 2015
- Meyer et al 2016

###TODO:
This is work in progress, ideally the following papers will all also be indexed


#### other large ancient
- Mittnik et al 2017
- Olalde et al 2017
- Mathieson et al 2017
- Lipson et al 2017
- Lindo et al 2016
- Lazaridis et al 2016
- Raghavan et al 2015
- Mathieson et al. 2015
- Haak et al 2015
- Allentoft et al 2015

####Ancient Modern Humans older than 10kya
- Fu et al 2014
- Raghavan et al 2014 (Mal'ta)
- Rasmussen et al 2014
- Seguin-Orlando et al 2014
- Fu et al 2015
- Jones et al 2015
- Rasmussen et al. 2015
- Fu et al. 2016
- Lazaridis et al 2016
- Jones et al. 2017
- Lindo et al 2017
- Mathieson et al. 2017
- Yang et al. 2017

#### Sediments
- Slon et al. 2017 (Sediment)

##Install
The repo only contains code to fetch and split up files, not any files itself
(for copyright reasons). It requires

- `unix-like filesystem`
- `wget`
- `python3 and snakemake`

If you don't have snakemake installed, you can install it from pypi using  
`pip install snakemake`. To populate the repository, use the following commands

```bash
bash download.sh
snakemake all
```

